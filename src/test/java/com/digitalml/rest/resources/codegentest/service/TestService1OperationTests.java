package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TestService1.TestService1ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TestService1Service.TestService1OperationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TestService1Service.TestService1OperationReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class TestService1OperationTests {

	@Test
	public void testOperationTestService1OperationBasicMapping()  {
		TestService1ServiceDefaultImpl serviceDefaultImpl = new TestService1ServiceDefaultImpl();
		TestService1OperationInputParametersDTO inputs = new TestService1OperationInputParametersDTO();
		inputs.setRequestModel(org.apache.commons.lang3.StringUtils.EMPTY);
		TestService1OperationReturnDTO returnValue = serviceDefaultImpl.testService1Operation(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}